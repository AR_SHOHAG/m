#include <stdio.h>
#include <stdlib.h>
#define MAX 50

void pascal(int mat[][MAX], int rows)
{
  int i, j;
  for(i = 0; i < rows; i++){
     mat[i][0] = 1;
     for(j = 1; j <= i - 1; j++)
        mat[i][j] = mat[i - 1][j - 1] + mat[i - 1][j];

     if (i != 0)
	 mat[i][i] = 1;
    }
}


int main ()
{

      int T, N, L, k;

      scanf("%d", &T);
      for(k=1; k<=T; ++k){
          int arr[MAX][MAX];
          scanf ("%d %d", &L, &N);
          pascal(arr, L);
          printf("Case %d: %d\n", k, arr[L-1][N-1]);
      }
      return 0;
}


